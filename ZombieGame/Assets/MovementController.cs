﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public float movementSpeed = 10f;
    public float rotationSpeed = 10f;
    public float jumpForce = 8f;
    public float gravity = 30f;
    public float angle = 0f;


    private Vector3 moveDir = Vector3.zero;
    private CharacterController controller;
    private Transform cameraTransform;
    
    private float timer = 0.0f;
    private float bobbingSpeed = 0.18f;
    private float bobbingAmount = 0.1f;
    private float midpoint = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        controller = gameObject.GetComponent<CharacterController>();

        cameraTransform = Camera.main.transform;
        cameraTransform.position = transform.position;

        midpoint = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        Walk();
        Rotate();
        HeadBobbing();
    }

    void Rotate()
    {
        float minAngle = -90;
        float maxAngle = 90;

        //Horizontal Rotation
        transform.Rotate(0, Input.GetAxis("Mouse X") * rotationSpeed, 0);

        //Vertical Rotation
        angle -= (Input.GetAxis("Mouse Y") * rotationSpeed);
        angle = Mathf.Clamp(angle, minAngle, maxAngle);
        cameraTransform.eulerAngles = new Vector3(
            angle,
            transform.rotation.eulerAngles.y,
            transform.rotation.eulerAngles.z
            );
    }

    void Walk()
    {
        if (controller.isGrounded)
        {
            //Walk on the GROUND

            moveDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

            moveDir = transform.TransformDirection(moveDir);

            moveDir *= movementSpeed;

            if (Input.GetButtonDown("Jump"))
            {
                moveDir.y = jumpForce;
            }
        }
        //Gravity always applied

        moveDir.y -= gravity * Time.deltaTime;

        controller.Move(moveDir * Time.deltaTime);
    }

    //Camera Heab BOB 
    void HeadBobbing()
    {
        float waveslice = 0.0f;
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");
        if (Mathf.Abs(horizontal) == 0 && Mathf.Abs(vertical) == 0)
        {
            timer = 0.0f;
        }
        else
        {   //Sin VALUE
            waveslice = Mathf.Sin(timer);
            timer = timer + bobbingSpeed;
            if (timer > Mathf.PI * 2)
            {
                timer = timer - (Mathf.PI * 2);
            }
        }
        if (waveslice != 0)
        {
            var translateChange = waveslice * bobbingAmount;
            var totalAxes = Mathf.Abs(horizontal) + Mathf.Abs(vertical);
            totalAxes = Mathf.Clamp(totalAxes, 0.0f, 1.0f);
            translateChange = totalAxes * translateChange;
            cameraTransform.localPosition = new Vector3(cameraTransform.localPosition.x, midpoint + translateChange, cameraTransform.localPosition.z);
        }
        else
        {   //Rest Position -> Sin 0
            cameraTransform.localPosition = new Vector3(cameraTransform.localPosition.x, midpoint, cameraTransform.localPosition.z);
        }
    }
}
